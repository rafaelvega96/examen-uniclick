from fastapi import FastAPI
import csv

app = FastAPI()

@app.get("/")
async def main():
    emails = []
    filtered_emails = []
    #for input I use a csv file to read the emails
    with open("./emails.csv", 'r') as file:
        csvreader = csv.reader(file)
        for row in csvreader:
            emails += row             
    filtered_emails = filter_email(emails)
    filtered_emails.sort()

    print(filtered_emails)
    return filtered_emails

#filter function
def filter_email(emails) :
    return list(filter(profileEmail, emails))

#return True if email is valid.  
def profileEmail(email):
    try:
        #split the email in parts 
        username, url = email.split("@")
        website, extension = url.split('.')
    except ValueError:
        return False
    #return True if the us∂ername only has alphanumeric caracteres 
    if username.replace("-","").replace("_","").isalnum() is False:
        return False
    #return False if the username only has alphanumeric caracteres 
    elif website.isalnum() is False:
        return False 
    #return False 
    elif len(extension) > 3:
        return False

    else:
        return True











