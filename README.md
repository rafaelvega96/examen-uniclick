
# Overview

## Validating Email Address With a Filter

You are given an integer N followed by N email address. Your task is to print a list containing only valid email addresses in lexicographical order.

Valid email addresses must follow these rules:

* It must have the username@websitename.extension format type

* The username can only contain letter, digits, dashes and underscores.

* The website name can only have letter and digits.

* The maxium length of the extension is 3. 

Sample output

['brian-23@hackerrank.com', 'britts_54@hackerrank.com', 'lara@hackerrank.com']

# Quick start

To get this project up and running locally on your computer: 

1. Set up the Pyhton development enviroment. for this project we use FastApi with uvicorn. 

1. Asumming you have python setup and fastApi, run the following commmands
    ```  
    python3 -m uvicorn main:app --reload --port 5000
    ```
1. Open a browser to `http://127.0.0.1:5000` to see the main site and running the code. 

1. You can add in the email.cvs file emails if you want test. You must to re-run the project if you add more emails in the cvs file

